##Usage
    var express = require('express'),
    	express_velocity = require('./express_velocity.js');

    var app = express();

    app.configure(function(){
      app.set('views', __dirname + '/views');
      app.set('view engine', 'vm');
      app.engine('.vm',express_velocity.render);
    });

    app.get('/', function(req, res){
      res.render('index.vm', {
        title: 'Home'
      });
    });


